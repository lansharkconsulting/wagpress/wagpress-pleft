**********
Change Log
**********

All enhancements and patches to wagpress-base will be documented in this file. This project adheres to `Semantic Versioning <https://semver.org>`_.

[2018-07-21]
============

**Added**

* CONTRIBUTING
* CODE_OF_CONDUCT
* CONTRIBUTORS

**Changed**

* Updated the README
* Updated Project Features in the README

[2018-06-01]
============

initial commit (@lanshark)
